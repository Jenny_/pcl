<?php

abstract class Cucumber {
	
	private $resSock = NULL;
	protected $arrConfig;
	protected $arrEmotes;
	protected $arrErrors;
	protected $arrSafechat;
	protected $blnReceivedLogin = false;
	protected $objPhraseAPI;
	protected $strOldPacket = '';
	public $arrPlayers = array();
	public $arrServers;
	public $myPlayer;
	
	function addItem($intItem){
		$this->sendPacket('%xt%s%i#ai%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intItem . '%');
	}

	function connect($strUsername, $strPassword, $strServer){
		if($this->resSock !== NULL) return false;
		$this->resSock = socket_create(AF_INET, SOCK_STREAM, 0);
		socket_connect($this->resSock, $this->generateLoginIP(), $this->generateLoginPort($strUsername));
		$this->sendPacket('<policy-file-request/>');
		$this->sendPacket("<msg t='sys'><body action='verChk' r='0'><ver v='153' /></body></msg>");
		$this->sendPacket("<msg t='sys'><body action='rndK'r='-1'></body></msg>");
		$strKey = $this->generateKey($strPassword, $this->myPlayer->getProperty('rndK'), true);
		$strData = $this->sendPacket("<msg t='sys'><body action='login' r='0'><login z='w1'><nick><![CDATA[$strUsername]]></nick><pword><![CDATA[$strKey]]></pword></login></body></msg>");
		if(stripos($strData, 'xt%e')) return false;
		$this->log('Login successful');
		socket_close($this->resSock);
		/* Reconnecting process (this time we connect to the game) */
		$arrServer = $this->getServer($strServer);
		$this->resSock = socket_create(AF_INET, SOCK_STREAM, 0);
		socket_connect($this->resSock, $arrServer['ip'], $arrServer['port']);
		$this->sendPacket('<policy-file-request/>');
		$this->sendPacket("<msg t='sys'><body action='verChk' r='0'><ver v='153' /></body></msg>");
		$this->sendPacket("<msg t='sys'><body action='rndK'r='-1'></body></msg>");
		$strKey = $this->generateKey($this->myPlayer->getProperty('loginKey'), $this->myPlayer->getProperty('rndK'));
		$this->sendPacket("<msg t='sys'><body action='login' r='0'><login z='w1'><nick><![CDATA[$strUsername]]></nick><pword><![CDATA[$strKey]]></pword></login></body></msg>");
		$this->sendPacket('%xt%s%j#js%-1%' . $this->myPlayer->getProperty('id') . '%' . $this->myPlayer->getProperty('loginKey') . '%en%');
		$this->sendPacket('%xt%s%i#gi%-1%', true, 'gi');
		$this->log(sprintf('Successfully connected to %s (%s:%d)', $arrServer['name'], $arrServer['ip'], $arrServer['port']));
		unset($arrServer);
	}
	
	function encryptPassword($strPassword){
		$strHash = md5($strPassword);
		$strHash = substr($strHash, 16, 16) . substr($strHash, 0, 16);
		return $strHash;
	}
	
	function disconnect(){
		socket_close($this->resSock);
		self::__destruct();
	}
	
	function findBuddy($intPlayer){
		$this->sendPacket('%xt%s%u#bf%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intPlayer . '%');
	}
	
	function generateKey($strPassword, $strRndK, $blnIsLogin = false){
		$strKey = $blnIsLogin ? $this->encryptPassword(strtoupper($this->encryptPassword($strPassword)) . $strRndK . "Y(02.>'H}t\":E1") : $this->encryptPassword($strPassword . $strRndK) . $strPassword;
		return $strKey;
	}
	
	function generateLoginIP(){
		$intRand = rand(0, 3);
		switch($intRand){
			case 0:	return '204.75.167.218';
			case 1:	return '204.75.167.219';
			case 2:	return '204.75.167.176';
			case 3:	return '204.75.167.177';
		}
	}
	
	function generateLoginPort($strUsername){
		return ord($strUsername) % 2 ? 3724 : 6112;
	}
	
	function getServer($funcServer){
		if(is_numeric($funcServer) && isset($this->arrServers[$funcServer]))
			return array('id' => $funcServer, 'name' => $this->arrServers[$funcServer]['name'], 'ip' => $this->arrServers[$funcServer]['ip'], 'port' => $this->arrServers[$funcServer]['port'], 'is_safe' => $this->arrServers[$funcServer]['safe']);
		foreach($this->arrServers as $intServerID => $arrServer){
			if($arrServer['name'] == $funcServer)
				return array('id' => $intServerID, 'name' => $funcServer, 'ip' => $arrServer['ip'], 'port' => $arrServer['port'], 'is_safe' => $arrServer['safe']);
		} return null;
	}
	
	function handlePacket($strData){
		return stripos($strData, '<k>') ? $this->myPlayer->setProperty('rndK', $this->stribet($strData, '<k>', '</k>')) : stripos($strData, 'xt') ? $this->parsePacket($strData) : false;
	}
	
	function joinRoom($intRoom, $intX = 0, $intY = 0){
		if($intRoom != $this->myPlayer->getProperty('extRoomID')){
			$this->sendPacket('%xt%s%j#jr%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intRoom . '%' . $intX . '%' . $intY . '% ', true, 'jr');
		}
	}
	
	function log($strText, $strType = 'INFO'){
		echo '[', date('H\:i\:s'), '][', $strType, '] > ', $strText, chr(10);
	}
	
	function parsePacket($strData){
		if(!stripos($strData, 'xt')) return false;
		$arrPacket = explode('%', $strData);
		unset($arrPacket[0]);
		array_pop($arrPacket);
		if($this->arrConfig['Debug']['Misc'] == true)
		print_r($arrPacket);
		switch($arrPacket[2]){
			case 'ap':
				$intPlayer = substr($arrPacket[4], 0, stripos($arrPacket[4], '|'));
				$this->updatePlayerCrumbs($intPlayer, $arrPacket[4]);
				$this->updatePlayerProperty($intPlayer, 'extRoomID', $this->myPlayer->getProperty('extRoomID'));
				if($this->arrConfig['Debug']['Handle'] == true && $intPlayer != $this->myPlayer->getProperty('id'))
				$this->log(sprintf('%s joined the room', $this->arrPlayers[$intPlayer]->getProperty('name')));
			break;
			case 'e':
				$this->log(sprintf('Received error: %d (%s)', $arrPacket[4], $this->arrErrors[$arrPacket[4]] ? $this->arrErrors[$arrPacket[4]]['desc'] : 'Unknown error'));
			break;
			case 'gi':
				$arrItems = array();
				$intItems = sizeof($arrPacket);
				for($intItem = 4; $intItem < $intItems; $intItem++){
					$intItemID = $arrPacket[$intItem];
					if($intItemID != '') $arrItems[] = $arrPacket[$intItem];
				}
				$this->myPlayer->setProperty('items', $arrItems);
				unset($arrItems);
				$this->log('Received inventory');
			break;
			case 'jr':
				$this->myPlayer->setProperty('intRoomID', $arrPacket[3]);
				$this->myPlayer->setProperty('extRoomID', $arrPacket[4]);
				for($i = 5; $i < sizeof($arrPacket); $i++){
					$intPlayer = substr($arrPacket[$i], 0, strpos($arrPacket[$i], '|'));
					$this->updatePlayerCrumbs($intPlayer, $arrPacket[$i]);
					$this->updatePlayerProperty($intPlayer, 'extRoomID', $arrPacket[4]);
				}
				$this->log(sprintf('Joined room: %d', $arrPacket[4]));
			break;
			case 'l':
				if($this->blnReceivedLogin == true) break;
				$this->log('Received login packet.');
				$this->myPlayer->setProperty('id'		, $arrPacket[4]);
				$this->myPlayer->setProperty('uuid'		, $arrPacket[5]);
				$this->myPlayer->setProperty('loginKey'	, $arrPacket[6]);
				$this->blnReceivedLogin = true;
			break;
			case 'lp':
				$this->myPlayer->parsePlayerObject($arrPacket[4]);
				$this->myPlayer->setProperty('coins', $arrPacket[5]);
				$this->myPlayer->setProperty('is_safe', $arrPacket[6]);
				$this->myPlayer->setProperty('egg_timer_remaining', $arrPacket[7]);
				$this->myPlayer->setProperty('server_join_time', $arrPacket[8]);
				$this->myPlayer->setProperty('age', $arrPacket[9]);
				$this->myPlayer->setProperty('banned_age', $arrPacket[10]);
				$this->myPlayer->setProperty('minutes_played', $arrPacket[11]);
				$this->myPlayer->setProperty('membership_days_remaining', $arrPacket[12]);
				$this->myPlayer->setProperty('timezone_offset', 0 - $arrPacket[13]);
				$this->log('Loaded player');
				if($this->arrConfig['Debug']['Misc'] == true)
				$this->myPlayer->returnArray();
			break;
			case 'rp':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s left the room', $this->arrPlayers[$intPlayer]->getProperty('name')) : sprintf('%d left the room', $intPlayer));
				# Remove from players array?
			break;
			case 'sa':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s sent action %d', $this->arrPlayers[$intPlayer]->getProperty('name'), $arrPacket[5]) : sprintf('%d sent action %d', $intPlayer, $arrPacket[5]));
			break;
			case 'sb':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s threw snowball to %d, %d', $this->arrPlayers[$intPlayer]->getProperty('name'), $arrPacket[5], $arrPacket[6]) : sprintf('%d threw snowball to %d, %d', $intPlayer, $arrPacket[5], $arrPacket[6]));
			break;
			case 'sc':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s said "%s"', $this->arrPlayers[$intPlayer]->getProperty('name'), $this->objPhraseAPI->strTranslate($arrPacket[5])) : sprintf('%d said "%s"', $intPlayer, $this->objPhraseAPI->strTranslate($arrPacket[5])));
			break;
			case 'se':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s emoted %s', $this->arrPlayers[$intPlayer]->getProperty('name'), $this->arrEmotes[$arrPacket[5]]) : sprintf('%d emoted %s', $intPlayer, $arrPacket[5]));
			break;
			case 'sf':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s sent frame %d', $this->arrPlayers[$intPlayer]->getProperty('name'), $arrPacket[5]) : sprintf('%d sent frame %d', $intPlayer, $arrPacket[5]));
			break;
			case 'sm':
				$intPlayer	= $arrPacket[4];
				if($this->arrConfig['Debug']['Handle'] == true)
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s said "%s"', $this->arrPlayers[$intPlayer]->getProperty('name'), $arrPacket[5]) : sprintf('%d said "%s"', $intPlayer, $arrPacket[5]));
			break;
			case 'sp':
				$intPlayer	= $arrPacket[4];
				$this->updatePlayerProperty($intPlayer, 'x', $arrPacket[5]);
				$this->updatePlayerProperty($intPlayer, 'y', $arrPacket[6]);
			break;
			case 'ss':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s sent safechat message "%s"', $this->arrPlayers[$intPlayer]->getProperty('name'), $this->arrSafechat[$arrPacket[5]]) : sprintf('%d sent safechat message "%s"', $intPlayer, $this->arrSafechat[$arrPacket[5]]));
			break;
			case 'upc':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their color to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their color to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'color', $arrPacket[4]);
			break;
			case 'uph':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their head item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their head item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'head', $arrPacket[4]);
			break;
			case 'upf':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their face item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their face item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'face', $arrPacket[4]);
			break;
			case 'upn':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their neck item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their neck item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'neck', $arrPacket[4]);
			break;
			case 'upb':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their body item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their body item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'body', $arrPacket[4]);
			break;
			case 'upa':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their hand item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their hand item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'hand', $arrPacket[4]);
			break;
			case 'upe':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their feet item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their feet item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'feet', $arrPacket[4]);
			break;
			case 'upl':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their pin item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their pin item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'flag', $arrPacket[4]);
			break;
			case 'upp':
				$intPlayer	= $arrPacket[4];
				$this->log(isset($this->arrPlayers[$intPlayer]) ? sprintf('%s changed their background item to %s', $this->arrPlayers[$intPlayer]->getProperty('name'), isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]) : sprintf('%d changed their background item to %s', $intPlayer, isset($this->arrItems[$arrPacket[5]]) ? $this->arrItems[$arrPacket[5]]['name'] : $arrPacket[5]));
				$this->updatePlayerProperty($intPlayer, 'photo', $arrPacket[4]);
			break;
			default:
				$this->log(sprintf('Unknown packet: %s', $strData), 'RECV');
			break;
		}
		unset($arrPacket);
	}
	
	function recvPacket($strNeedle = ''){
		$intTimeout = $this->arrConfig['Other']['RecvTimeout'];
		$longTime = time();
		$strData = '';
		Recv:
		do {
			socket_recv($this->resSock, $strData, 9000, 0);
		}
		while((time() + $intTimeout < $longTime) ^ ($strData === '' || ($this->strOldPacket != '' && $strData == $this->strOldPacket)));
		/* Work around for timing out too fast (not sure this works) */
		if(time() + $intTimeout < $longTime){
			goto Recv;
		} elseif($strData == ''){
			$this->log('Recv timed out.', 'RECV');
			return false;
		}
		$arrData = explode(chr(0), $strData);
		foreach($arrData as $strData){
			if($strData == '') continue;
			if($this->arrConfig['Debug']['Recv'] == true)
			$this->log(sprintf('Received Data: %s', $strData), 'RECV');
			$this->handlePacket($strData);
			$this->strOldPacket = $strData;
		}
		return $this->strOldPacket;
	}
	
	function sendPacket($strData, $blnRecv = true, $strNeedle = ''){
		# Initialize thread
		if(substr($strData, strlen($strData) -1, 1) != chr(0)) $strData .= chr(0);
		@socket_write($this->resSock, $strData, strlen($strData));
		if($this->arrConfig['Debug']['Send'] == true)
		$this->log(sprintf('Sending packet: %s', $strData));
		return $blnRecv == true && $strNeedle != '' ? $this->waitForNeedle($strNeedle) : $blnRecv == true && $strNeedle == '' ? $this->recvPacket() : true;
	}
	
	function sendFrame($intFrame){
		$this->sendPacket('%xt%s%u#sf%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intFrame . '%');
	}
	
	function sendMessage($strMessage){
		$strPhraseID = $this->objPhraseAPI->strTranslate($strMessage);
		$this->sendPacket($strPhraseID != '' ? '%xt%s%m#sc%' . $this->myPlayer->getProperty('intRoomID') . '%' . $this->myPlayer->getProperty('id') . '%' . $strPhraseID . '%' : '%xt%s%m#sm%' . $this->myPlayer->getProperty('intRoomID') . '%' . $this->myPlayer->getProperty('id') . '%' . $strMessage . '%');
	}
	
	function sendPosition($intX, $intY){
		$this->sendPacket('%xt%s%u#sp%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intX . '%' . $intY . '%');
	}
	
	function stribet($strHash, $strLeft, $strRight){
		$intLeft = stripos($strHash, $strLeft) + strlen($strLeft);
		$intRight = stripos($strHash, $strRight, $intLeft);
		return substr($strHash, $intLeft, $intRight - $intLeft);
	}
	
	function updatePlayerProperty($intPlayer, $strProperty, $strValue){
		if($intPlayer == $this->myPlayer->getProperty('id')){
			return $this->myPlayer->setProperty($strProperty, $strValue);
		} elseif(isset($this->arrPlayers[$intPlayer])){
			return $this->arrPlayers[$intPlayer]->setProperty($strProperty, $strValue);
		} else {
			$this->arrPlayers[$intPlayer] = new Player();
			return $this->arrPlayers[$intPlayer]->setProperty($strProperty, $strValue);
		}
	}
	
	/* Updates player crumbs by ID using $strCrumbs (string with vertical lines) */
	function updatePlayerCrumbs($intPlayer, $strCrumbs){
		if($intPlayer == $this->myPlayer->getProperty('id')){
			return $this->myPlayer->parsePlayerObject($strCrumbs);
		} elseif(isset($this->arrPlayers[$intPlayer])){
			return $this->arrPlayers[$intPlayer]->parsePlayerObject($strCrumbs);
		} else {
			$this->arrPlayers[$intPlayer] = new Player();
			return $this->arrPlayers[$intPlayer]->parsePlayerObject($strCrumbs);
		}
	}
	
	function updatePlayerItem($strType, $intItem){
		$this->sendPacket('%xt%s%s#up' . strtolower($strType) . '%' . $this->myPlayer->getProperty('intRoomID') . '%' . $intItem . '%');
	}
	
	/* Like recvPacket but w/ no timeouts and has to receive a packet with $strNeedle */
	/* Should I disallow the acceptance of old packets? */
	function waitForNeedle($strNeedle = ''){
		if($strNeedle == '') return false;
		$strData = '';
		while($strData === '' ^ !stripos($strData, $strNeedle)){
			socket_recv($this->resSock, $strData, 9000, 0);
		}
		$arrData = explode(chr(0), $strData);
		foreach($arrData as $strData){
			if($strData == '') continue;
			if($this->arrConfig['Debug']['Recv'] == true)
			$this->log(sprintf('Received Data: %s', $strData), 'RECV');
			$this->handlePacket($strData);
			$this->strOldPacket = $strData;
		}
		return $this->strOldPacket;
	}
	
	function __destruct(){
		return @socket_close($this->resSock);
	}
	
}

?>
