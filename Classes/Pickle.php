<?php
declare(ticks = 1100);
function __autoload($strClass){
	(@include sprintf('%s.php', sprintf('%s/%s', __DIR__, $strClass))) || die(sprintf('%s.php', sprintf('%s/%s', __DIR__, $strClass)));
}
class Pickle extends Cucumber {
	
	function __construct(){
		date_default_timezone_set(@date_default_timezone_get());
		/* Add INI file parsing */
		$this->arrConfig	= parse_ini_file('Config/PCL.conf',	true);
		$this->arrEmotes	= parse_ini_file('INI/emotes.ini'		);
		$this->arrErrors	= parse_ini_file('INI/errors.ini',	true);
		$this->arrItems		= parse_ini_file('INI/items.ini',	true);
		$this->arrSafechat	= parse_ini_file('INI/safechat.ini',true);
		$this->arrServers	= parse_ini_file('INI/servers.ini',	true);
		$this->myPlayer		= new Player();
		$this->objPhraseAPI	= new PhraseChat();
		register_tick_function(array($this, $this->arrConfig['Other']['TickHandler']));
		$this->log(sprintf('%s initiated.', __CLASS__));
	}
	
	function handleTick(){
		$this->log('Sent heartbeat', 'SEND');
		return $this->sendPacket('%xt%u#h%' . $this->myPlayer->getProperty('intRoomID') . '%' . $this->myPlayer->getProperty('id') . '%', false);
	}
	
}

?>
