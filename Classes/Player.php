<?php

class Player {
	
	private $arrProperties;
	
	public function getProperty($strProperty){
		return isset($this->arrProperties[$strProperty]) ? $this->arrProperties[$strProperty] : false;
	}
	
	function parsePlayerObject($strPacket){
		if($strPacket == '') return false;
		$arrInfo = explode('|', $strPacket);
		$this->setProperty('id'						, $arrInfo[0]); # I know what you're thinking.. "isn't this already set?" the answer is "no." ;)
		$this->setProperty('name'					, $arrInfo[1]);
		$this->setProperty('language_bitmask'		, $arrInfo[2]);
		$this->setProperty('color'					, $arrInfo[3]);
		$this->setProperty('head'					, $arrInfo[4]);
		$this->setProperty('face'					, $arrInfo[5]);
		$this->setProperty('neck'					, $arrInfo[6]);
		$this->setProperty('body'					, $arrInfo[7]);
		$this->setProperty('hands'					, $arrInfo[8]);
		$this->setProperty('feet'					, $arrInfo[9]);
		$this->setProperty('flag'					, $arrInfo[10]);
		$this->setProperty('photo'					, $arrInfo[11]);
		$this->setProperty('x'						, $arrInfo[12]);
		$this->setProperty('y'						, $arrInfo[13]);
		$this->setProperty('frame'					, $arrInfo[14]);
		$this->setProperty('is_member'				, $arrInfo[15]);
		$this->setProperty('total_membership_days'	, $arrInfo[16]);
		unset($arrInfo);
	}
	
	public function returnArray(){
		return print_r($this->arrProperties);
	}
	
	public function setProperty($strProperty, $strValue){
		$this->arrProperties[$strProperty] = $strValue;
	}
	
	function __destruct(){
		unset($this->arrProperties);
	}
	
}

?>
